import React from 'react';
import logo from './logo.svg';
import './App.css';
import "react-table-v6/react-table.css";
import Booking from './Booking';

function App() {
  return (
    <div className="App">
      <header className="App-header">
     <Booking />
      </header>
    </div>
  );
}

export default App;
