import React, { useState, useEffect } from "react";
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";

const Booking = props => {
  
   const [booking, setBooking] = useState([]);
    useEffect(() => {
        fetch("/bookings")
        .then(function (response) {
            response.json().then(function (data){
                setBooking(data);
            })
        })
    },[]);
    const columns =[
    {
        Header: "Id",
        accessor: "id"
    }, 
    {
        Header: "date",
        accessor: "date"
    }, 
    {
        Header: "Origin",
        accessor:"origin"
    }, 
    {
        Header: "Destination",
        accessor: "destination"
    }
];

    return (
        <div>
            <h4>{props.title}</h4>
            <ReactTable data={booking} columns={columns} />
        </div>
    );
};

export default Booking;